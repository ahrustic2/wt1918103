let mapaPocetnihDana = new Map([[0,  1], [1,  4], [2,  4], [3,  0], [4,  2], [5,  5], [6,  0], [7, 3], [8,  6],[9,  1], [10,  4], [11,  6]]);
let mjesecSale;
let godina;
const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const fs = require('fs');

const app = express();
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static('js'));
app.use(express.static('css'));


// Inicijalizacija baze
const db = require('./db.js')
db.sequelize.sync({force:true}).then(function(){
     inicijalizacija();
     console.log("Kreirana i popunjena baza");
});
//Osoblje 
app.get ('/osoblje' , function (req, res) {
    db.Osoblje.findAll({ attributes: ['ime', 'prezime', 'uloga']}).then(function(osobe) {
        res.json(osobe);
      });
   });

app.get ('/sale' , function (req, res) {
    db.Sala.findAll({ attributes: ['naziv', 'zaduzenaOsoba']}).then(function(sale) {
        res.json(sale);
      });
   });   

app.get ('/zauzeca' , function (req, res) {
    db.Rezervacija.findAll({ attributes: ['osoba', 'termin', 'sala']}).then(function(rezervacija) {
        res.json(rezervacija);
      });
   });   
   

app.get ('/termin' , function (req, res) {
    db.Termin.findAll({ attributes: ['redovni', 'dan', 'datum', 'semestar', 'pocetak', 'kraj', ]}).then(function(termin) {
        res.json(termin);
      });
   });    

app.get ('/osobljeZaText' , function (req, res) {
    db.Osoblje.findAll({ attributes: ['ime', 'prezime', 'uloga']}).then(function(osobe) {
        res.json(osobe);
      });
   });   

   app.get('/osobljeISale',function (req, res) {
    
    let podaciOsobljeSala = {};
    let podaci = [];
    let pomocnaDatum = new Date();
    let danasnjiDatum = new Date(2019, pomocnaDatum.getMonth(), pomocnaDatum.getDate(), pomocnaDatum.getHours(), pomocnaDatum.getMinutes());
    let trenutniSati = danasnjiDatum.getHours();
    let trenutneMinute = danasnjiDatum.getMinutes();
    let trenutniDan = danasnjiDatum.getDay();
    trenutniDan = dajDan(trenutniDan);
    db.Rezervacija.findAll({
      include: [
          {
              model: db.Termin
          },
         {
              model: db.Sala
          },
          {
              model: db.Osoblje
          } 
      ]
    }).then (function(lista){
        lista.forEach(function(rez){
             if (!rez.Termin.redovni) {
                let datumIzTermina = razdvoji(rez.Termin.datum);
                if (datumIzTermina.dan == danasnjiDatum.getDate() && datumIzTermina.mjesec == (danasnjiDatum.getMonth()))
                {   
                    if (danasnjiUnutarTermina(rez.Termin.pocetak, rez.Termin.kraj, trenutniSati, trenutneMinute) == 1)
                    {
                        podaci.push({ime: rez.Osoblje.ime, prezime: rez.Osoblje.prezime, uloga: rez.Osoblje.uloga, naziv: rez.Sala.naziv});
                    }
                }
             }
             else {

                if (rez.Termin.semestar == "ljetni" || rez.Termin.semestar == "zimski")
                {
                    if (rez.Termin.dan == trenutniDan)
                    {
                        if (danasnjiUnutarTermina(rez.Termin.pocetak, rez.Termin.kraj, trenutniSati, trenutneMinute) == 1)
                        {
                            podaci.push({ime: rez.Osoblje.ime, prezime: rez.Osoblje.prezime, uloga: rez.Osoblje.uloga, naziv: rez.Sala.naziv});
                  }
                    }
                }
             }
        });
        podaciOsobljeSala["podaci"] = podaci;
        res.json(podaciOsobljeSala);
    });
 });   


let podaciIzBaze ={};
//Rezervacije
app.get('/rezervacijeBaza',function (req, res) {
    
    let periodicneRezervacije = [];
    let vanrednaRezervacija=[];

    db.Rezervacija.findAll({
      include: [
          {
              model: db.Termin
          },
         {
              model: db.Sala
          },
          {
              model: db.Osoblje
          } 
      ]
    }).then (function(lista){
        lista.forEach(function(rez){
             if (!rez.Termin.redovni) {
                vanrednaRezervacija.push({datum: rez.Termin.datum, pocetak: rez.Termin.pocetak,
                kraj: rez.Termin.kraj, naziv: rez.Sala.naziv, predavac: rez.Osoblje.ime + " " + rez.Osoblje.prezime, 
                uloga:rez.Osoblje.uloga});
             }
             else {
                periodicneRezervacije.push({dan: rez.Termin.dan , semestar: rez.Termin.semestar, 
                pocetak: rez.Termin.pocetak, kraj: rez.Termin.kraj , naziv: rez.Sala.naziv, 
                predavac: rez.Osoblje.ime + " " + rez.Osoblje.prezime, uloga: rez.Osoblje.uloga });
             }
        });
    
        podaciIzBaze["periodicna"] = periodicneRezervacije;
        podaciIzBaze["vanredna"] = vanrednaRezervacija;
        res.json(podaciIzBaze);
    });
 });

//za pocetnu
app.get("/pocetna.html", function (req, res) {
    res.sendFile(__dirname + "/pocetna.html");
});

app.get("/testoviS4.html", function (req, res) {
    res.sendFile(__dirname + "/testoviS4.html");
});

app.get('/',function(req,res){
    res.sendFile(__dirname+"/pocetna.html");
 });
 
//za rezervaciju
app.get("/rezervacija.html", function (req, res) {
    res.sendFile(__dirname + "/rezervacija.html");
});
    
//za sale
app.get("/sale.html", function (req, res) {
    res.sendFile(__dirname + "/sale.html");
});
    
//za unos
app.get("/unos.html", function (req, res) {
    res.sendFile(__dirname + "/unos.html");
});

//za osoblje
app.get("/osoblje.html", function (req, res) {
    res.sendFile(__dirname + "/osoblje.html");
});
    
app.get("/index.js", function (req, res) {
    res.sendFile(__dirname + "/index.js");
});

app.get("/pozivi.js", function (req, res) {
    res.sendFile(__dirname + "/pozivi.js");
});

app.get("/zauzeca.json", function (req, res) {
    res.sendFile(__dirname + "/zauzeca.json");
});

app.get('/slike/logo.png', function (req, res) {
    res.sendFile(__dirname + '/slike/logo.png');
});

app.get('/pocetnaStrana', function (req, res) {
    for (let i = 1; i < 4; i++)
    {
        app.get('/slike/sala' + i +'.jpg', function (req, res) {
            res.sendFile(__dirname + '/slike/sala' + i +'.jpg');
        });
    }
    res.write("/slike/sala1.jpg,/slike/sala2.jpg,/slike/sala3.jpg");
    res.send();
});

app.get('/stranica2', function (req, res) {
    for (let i = 4; i < 7; i++)
    {
        app.get('/slike/sala' + i +'.jpg', function (req, res) {
            res.sendFile(__dirname + '/slike/sala' + i +'.jpg');
        });
    }
    res.write("/slike/sala4.jpg,/slike/sala5.jpg,/slike/sala6.jpg");
    res.send();
});

app.get('/stranica3', function (req, res) {
    for (let i = 7; i < 10; i++)
    {
        app.get('/slike/sala' + i +'.jpg', function (req, res) {
            res.sendFile(__dirname + '/slike/sala' + i +'.jpg');
        });
    }
    res.write("/slike/sala7.jpg,/slike/sala8.jpg,/slike/sala9.jpg");
    res.send();
});

app.get('/stranica4', function (req, res) {
    app.get('/slike/sala10.jpg', function (req, res) {
        res.sendFile(__dirname + '/slike/sala10.jpg');
    });
    res.write("/slike/sala10.jpg");
    res.send();
});

//#region
/*app.post('/novaRezervacija',function(req,res){
    let dodajSalu = false;
    fs.readFile('zauzeca.json', 'utf8' , (greska, data) => {
        if(greska) throw greska;
        let podaci = data.toString('utf-8');
        var contents = fs.readFileSync("zauzeca.json");
        var jsonContent = JSON.parse(contents);
        let periodicne = jsonContent.periodicna;
        let vanredne = jsonContent.vanredna;

        //Za periodicnu
        let pom1 = req.body['datum'].split(".");
        let danSaleKojaSeDodaje = pom1[0];
        mjesecSale = pom1[1];
        let pom2Mjesec = mjesecSale;
        mjesecSale--;
        mjesecSale = parseInt(mjesecSale);
        pom2Mjesec = parseInt(pom2Mjesec);
        godina = 2019;

        let semestarRezervacije = "";
        if(mjesecSale >=1 && mjesecSale <= 5) {
            semestarRezervacije = "ljetni";
        } else if(mjesecSale == 0 || (mjesecSale >= 9 && mjesecSale <= 11)) {
            semestarRezervacije = "zimski";
        }
        
        let noviDatum = pom2Mjesec + "." + danSaleKojaSeDodaje + "." + godina;
        let datumPomocna = new Date(noviDatum);
        let danSale = datumPomocna.getDay();
        danSale = dajDan(danSale);
        
        for (let i = 0; i < vanredne.length; i++)
        {
            //za validaciju vanredne i vanredne
            if (vanredne[i].naziv == req.body['naziv'] && vanredne[i].datum == req.body['datum'])
            {
                //koristim funkciju iz kalendara, ako se vrijeme ne poklapa dodamo 
                if (unutarVremena(req.body['pocetak'], req.body['kraj'], vanredne[i].pocetak, vanredne[i].kraj) == 1)
                {
                    dodajSalu = true;
                    break;
                    
                }
            }
            
            //prolazi kroz listu vanrednih i provjerava na koji su dan rezervisane
            if (req.body['periodicnaRezervacija'] == 1)
            {
                if (vanredne[i].naziv == req.body['naziv'])
                {
                    
                    let p1 = vanredne[i].datum.split(".");
                    let dan1 = p1[0];
                    let mjesec1 = p1[1];
                    mjesec1 = parseInt(mjesec1);
                    let godina1 = 2019;

                    let novi1 = mjesec1 + "." + dan1 + "." + godina1;
                    let novi1Pom = new Date(novi1);
                    let dan2 = novi1Pom.getDay();
                    dan2 = dajDan(dan2);

                    console.log("oooo" + mjesec1);
                    let semestarSaleIzListe = "";
                    if(mjesec1 >=2 && mjesec1 <= 6) {
                        semestarSaleIzListe = "ljetni";
                    } else if(mjesec1 == 1 || (mjesec1 >= 10 && mjesec1 <= 12)) {
                        semestarSaleIzListe = "zimski";
                    }
        
                    if (unutarVremena(req.body['pocetak'], req.body['kraj'], vanredne[i].pocetak, vanredne[i].kraj) == 1)
                    {
                        if (dan2 == danSale && semestarRezervacije == semestarSaleIzListe)
                        {
                            dodajSalu = true;
                            break;
                        } 
                    }
                }
            }
        }

        //Ako je periodicna i van semestara
        if (req.body['periodicnaRezervacija'] == 1)
        {
            if (semestarRezervacije == "")
            {
                dodajSalu = true;
            }    
        }

    
        for (let j = 0; j < periodicne.length; j++)
        {
            //provjera naziva sala
            if (periodicne[j].naziv == req.body['naziv'] && periodicne[j].semestar == semestarRezervacije)
            {
                    //provjera da li se vrijeme poklapa
                    if (unutarVremena(req.body['pocetak'], req.body['kraj'], periodicne[j].pocetak, periodicne[j].kraj) == 1)
                    {
                        //provjera da li su na isti dan unutar sedmice
                        if (danSale == periodicne[j].dan)
                        {
                            dodajSalu = true;
                            break;
                        }
                    }
            }
        }
         
        if (dodajSalu == false)
        {
            if (semestarRezervacije != "")
            {
                if (req.body['periodicnaRezervacija'] == 1)
                {
                    let pomocnaSala = { dan : danSale, semestar : semestarRezervacije, pocetak : req.body['pocetak'], kraj : req.body['kraj'],
                                    naziv : req.body['naziv'],  predavac : req.body['predavac']};
                    jsonContent.periodicna.push(pomocnaSala);
                }

                else
                {
                    let pomocnaSala = req.body;
                    delete pomocnaSala["periodicnaRezervacija"];
                    jsonContent.vanredna.push(pomocnaSala);
                }
            }

           else
            {
                let pomocnaSala = req.body;
                delete pomocnaSala["periodicnaRezervacija"];
                jsonContent.vanredna.push(pomocnaSala);
            }
            fs.writeFile("zauzeca.json", JSON.stringify(jsonContent),function(err, result) {
                if(err) console.log('error', err);
                jsonContent.ispravno = 1;
                res.json(jsonContent);
            });
        }
        else 
        {
            jsonContent.ispravno = 0;
            res.json(jsonContent);
        }

      }); 
 });*/
//#endregion

app.post('/novaRezervacija',function(req,res){
    let dodajSalu = false;
   
        //Za periodicnu
        let predavac = "";
        let periodicne = podaciIzBaze.periodicna;
        let vanredne = podaciIzBaze.vanredna;
        let pom1 = req.body['datum'].split(".");
        let danSaleKojaSeDodaje = pom1[0];
        mjesecSale = pom1[1];
        let pom2Mjesec = mjesecSale;
        mjesecSale--;
        mjesecSale = parseInt(mjesecSale);
        pom2Mjesec = parseInt(pom2Mjesec);
        godina = 2019;

        let semestarRezervacije = "";
        if(mjesecSale >=1 && mjesecSale <= 5) {
            semestarRezervacije = "ljetni";
        } else if(mjesecSale == 0 || (mjesecSale >= 9 && mjesecSale <= 11)) {
            semestarRezervacije = "zimski";
        }
        
        let noviDatum = pom2Mjesec + "." + danSaleKojaSeDodaje + "." + godina;
        let datumPomocna = new Date(noviDatum);
        let danSale = datumPomocna.getDay();
        danSale = dajDan(danSale);
        
        for (let i = 0; i < vanredne.length; i++)
        {
            //za validaciju vanredne i vanredne
            if (vanredne[i].naziv == req.body['naziv'] && vanredne[i].datum == req.body['datum'])
            {
                //koristim funkciju iz kalendara, ako se vrijeme ne poklapa dodamo 
                if (unutarVremena(req.body['pocetak'], req.body['kraj'], vanredne[i].pocetak, vanredne[i].kraj) == 1)
                {
                    predavac = vanredne[i].predavac + " ( " + vanredne[i].uloga + " ) ";
                    dodajSalu = true;
                    break;
                    
                }
            }
            
            //prolazi kroz listu vanrednih i provjerava na koji su dan rezervisane
            if (req.body['periodicnaRezervacija'] == 1)
            {
                if (vanredne[i].naziv == req.body['naziv'])
                {
                    
                    let p1 = vanredne[i].datum.split(".");
                    let dan1 = p1[0];
                    let mjesec1 = p1[1];
                    mjesec1 = parseInt(mjesec1);
                    let godina1 = 2019;

                    let novi1 = mjesec1 + "." + dan1 + "." + godina1;
                    let novi1Pom = new Date(novi1);
                    let dan2 = novi1Pom.getDay();
                    dan2 = dajDan(dan2);

                    let semestarSaleIzListe = "";
                    if(mjesec1 >=2 && mjesec1 <= 6) {
                        semestarSaleIzListe = "ljetni";
                    } else if(mjesec1 == 1 || (mjesec1 >= 10 && mjesec1 <= 12)) {
                        semestarSaleIzListe = "zimski";
                    }
        
                    if (unutarVremena(req.body['pocetak'], req.body['kraj'], vanredne[i].pocetak, vanredne[i].kraj) == 1)
                    {
                        if (dan2 == danSale && semestarRezervacije == semestarSaleIzListe)
                        {
                            predavac = vanredne[i].predavac +  " ( " + vanredne[i].uloga + " ) ";
                            dodajSalu = true;
                            break;
                        } 
                    }
                }
            }
        }

        //Ako je periodicna i van semestara
        if (req.body['periodicnaRezervacija'] == 1)
        {
            if (semestarRezervacije == "")
            {
                dodajSalu = true;
            }    
        }

    
        for (let j = 0; j < periodicne.length; j++)
        {
            //provjera naziva sala
            if (periodicne[j].naziv == req.body['naziv'] && periodicne[j].semestar == semestarRezervacije)
            {
                    //provjera da li se vrijeme poklapa
                    if (unutarVremena(req.body['pocetak'], req.body['kraj'], periodicne[j].pocetak, periodicne[j].kraj) == 1)
                    {
                        //provjera da li su na isti dan unutar sedmice
                        if (danSale == periodicne[j].dan)
                        {
                            predavac = periodicne[i].predavac + " ( " + periodicne[i].uloga + " ) ";
                            dodajSalu = true;
                            break;
                        }
                    }
            }
        }
         
        if (dodajSalu == false)
        {
            dodajSalu = 0;
            if (semestarRezervacije != "")
            {
                if (req.body['periodicnaRezervacija'] == 1)
                {
                    let pomocnaSala = { dan : danSale, semestar : semestarRezervacije, pocetak : req.body['pocetak'], kraj : req.body['kraj'],
                                    naziv : req.body['naziv'],  predavacIme: req.body['predavacIme'], predavacPrezime: req.body['predavacPrezime'], uloga: req.body['uloga'], idOsobe: req.body['idOsobe'], periodicnaRezervacija:1};
                    dodajRezervacijuUBazu(pomocnaSala);

                }

                else
                {
                    let pomocnaSala = req.body;
                    dodajRezervacijuUBazu(pomocnaSala);
                }
            }

           else
            {
                let pomocnaSala = req.body;
                dodajRezervacijuUBazu(pomocnaSala);
                if (req.body['periodicnaRezervacija'] == 1) dodajSalu = 3;
            }
        }
        res.json({dodajSalu: dodajSalu, preda: predavac});
  module.exports = app;
 });


 module.exports = app;
app.listen(8080);  

function unutarVremena(pocetak, kraj, salaPocetak, salaKraj)
{
    let zauzeta = 0;
    if (pocetak > salaPocetak && pocetak < salaKraj) 
         zauzeta = 1;
    else if (pocetak < salaPocetak && kraj > salaPocetak)
        zauzeta = 1;
    else if (pocetak == salaPocetak || kraj == salaKraj)
        zauzeta = 1;    
    return zauzeta;    
}

function dajDan(dan)
{
    if(dan == 0) 
        {
            dan = 6;
        } else {
            dan--;
        }

        return dan;
}



function inicijalizacija()
{
    return new Promise(function (resolve, reject){//za osoblje
        db.Osoblje.create({
            id: 1,
            ime: 'Neko',
            prezime: 'Nekić',
            uloga: 'profesor'
        }).then (object =>{ db.Osoblje.create({
            id: 2,
            ime: 'Drugi',
            prezime: 'Neko',
            uloga: 'asistent'
        }).then (object => {
            db.Osoblje.create({
                id: 3,
                ime: 'Test',
                prezime: 'Test',
                uloga: 'asistent'
            }).then (object => {
                //za salu
                db.Sala.create({
                    naziv: '1-11',
                    zaduzenaOsoba: 1
                }).then (object => {
                    db.Sala.create({
                        naziv: '1-15',
                        zaduzenaOsoba: 2
                    }).then (object => {
                        //za termin
                        db.Termin.create({
                            redovni: false,
                            dan: null,
                            datum: '1.1.2019',
                            semestar: null,
                            pocetak: '12:00',
                            kraj: '13:00'
                        }).then (object => { db.Termin.create({
                            redovni: true,
                            dan: 0,
                            datum: null,
                            semestar: 'zimski',
                            pocetak: '13:00',
                            kraj: '14:00'
                        }).then (object => {
                    
                            //za rezervaciju
                            db.Rezervacija.create({
                                termin: 1,
                                sala: 1,
                                osoba: 1
                            })
                            db.Rezervacija.create({
                                termin: 2,
                                sala: 1,
                                osoba: 3
                            })});});});});});
        });});}
    )}

function dodajRezervacijuUBazu(salaKojuCemoDodati)
{
    let idZaTermin, idZaOsoblje, idZaSalu;
    let redovnaRezervacija = false;
    let danRezervacije = null;
    let datumRezervacije = null;
    let semestarRezervacije = null;

    if (salaKojuCemoDodati.periodicnaRezervacija == 1) 
    {
        redovnaRezervacija = true;
        danRezervacije = salaKojuCemoDodati.dan;
        semestarRezervacije = salaKojuCemoDodati.semestar;
    }
    else {datumRezervacije = salaKojuCemoDodati.datum;}
    db.Termin.create({
        redovni: redovnaRezervacija,
        dan: danRezervacije,
        datum: datumRezervacije,
        semestar: semestarRezervacije,
        pocetak: salaKojuCemoDodati.pocetak,
        kraj: salaKojuCemoDodati.kraj
    }).then(podaciTermin =>{
        idZaOsoblje = salaKojuCemoDodati.idOsobe;
        idZaTermin = podaciTermin.dataValues.id; 
        }).then(object =>{
            db.Sala.create({naziv: salaKojuCemoDodati.naziv, zaduzenaOsoba: idZaOsoblje}
            ).then(nadenaSala => {
                idZaSalu = nadenaSala.dataValues.id;
                db.Rezervacija.create({termin: idZaTermin, osoba: idZaOsoblje, sala:idZaSalu});
            });
        });
}

function razdvoji(datum)
{
    let pom1 = datum.split(".");
    let danSale = pom1[0];
    let mjesecSale = pom1[1];
    mjesecSale--;
    mjesecSale = parseInt(mjesecSale);

    return ({dan: danSale, mjesec: mjesecSale});
}

function danasnjiUnutarTermina(pocetak, kraj, sati, minute)
{
    var satiPocetak = pocetak.split(":")[0];
    var minutePocetak = pocetak.split(":")[1];

    var satiKraj = kraj.split(":")[0];
    var minuteKraj = kraj.split(":")[1];

    if (satiPocetak < sati && satiKraj > sati) return 1; //unutar
    if (satiPocetak > sati || satiKraj < sati) return 0; //van
    if (satiPocetak == sati || satiKraj == sati) 
    {
        if(satiKraj > sati) return 1;
        if (satiPocetak < sati) return 1;
        if (minutePocetak < minute && minuteKraj > minute) return 1;
        else if (minutePocetak == minute && minuteKraj > minute) return 1;
        else if (minutePocetak < minute && minuteKraj == minute) return 1;
        else return 0;
    }
}