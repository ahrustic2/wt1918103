const supertest = require("supertest");
const assert = require("assert");
const app = require("./index");

describe("GET /osoblje", function() {
  it("Status code 200", function(done) {
    supertest(app)
      .get("/osoblje")
      .expect(200)
      .end(function(err, res) {
        if (err) done(err);
        done();
      });
  });

  it("Provjera kada se baza ucita", function(done) {
    supertest(app)
      .get("/osoblje")
      .expect([
        { ime: 'Neko', prezime: 'Nekić', uloga: 'profesor' },
        { ime: 'Drugi', prezime: 'Neko', uloga: 'asistent' },
        { ime: 'Test', prezime: 'Test', uloga: 'asistent' }
      ])
      .end(function(err, res) {
        if (err) done(err);
        done();
      });  
  });

  it("Provjera osoblja za text", function(done) {
    supertest(app)
      .get("/osobljeZaText")
      .expect([
        { ime: 'Neko', prezime: 'Nekić', uloga: 'profesor' },
        { ime: 'Drugi', prezime: 'Neko', uloga: 'asistent' },
        { ime: 'Test', prezime: 'Test', uloga: 'asistent' }
      ])
      .end(function(err, res) {
        if (err) done(err);
        done();
      });  
      
  });

});

describe("GET /sale", function() {
  it("Status code 200", function(done) {
    supertest(app)
      .get("/sale")
      .expect(200)
      .end(function(err, res) {
        if (err) done(err);
        done();
      });
  });

  it("Provjera kada se baza ucita", function(done) {
    supertest(app)
      .get("/sale")
      .expect([
        { naziv: '1-11', zaduzenaOsoba: 1 },
        { naziv: '1-15', zaduzenaOsoba: 2 }
      ])
      .end(function(err, res) {
        if (err) done(err);
        done();
      });  
  });
});

describe("GET /zauzeca", function() {
  it("Status code 200", function(done) {
    supertest(app)
      .get("/zauzeca")
      .expect(200)
      .end(function(err, res) {
        if (err) done(err);
        done();
      });
  });

  it("Provjera kada se baza ucita", function(done) {
    supertest(app)
      .get("/zauzeca")
      .expect( [ { osoba: 1, termin: 1, sala: 1 }, { osoba: 3, termin: 2, sala: 1 } ])
      .end(function(err, res) {
        if (err) done(err);
        done();
      });  
  });
});

describe("POST /", function(){
  it("Provjera POST", function(done) {
    supertest(app)
      .post("/novaRezervacija")
      .send({periodicnaRezervacija : 0, datum:'18.1.2019', 
      pocetak:'10:00', kraj:'15:00', naziv:'0-01', 
      predavacIme: 'Neko', predavacPrezime: 'Nekić', uloga: 'profesor', idOsobe: '1'})
      .expect(500)
      .end(function(err, res){
        if (err) done(err);
        done();
      });
  });

  it("Pogresni podaci (ocekivano je da baci gresku split od null zato sto saljemo null)", function(done) {
    supertest(app)
      .post("/novaRezervacija")
      .send({periodicnaRezervacija : null, datum:null, 
      pocetak:null })
      .expect(500)
      .end(function(err, res) {
        if (err) done(err);
        done();
      });
  });

  it("Pogresni podaci (ocekivano je da baci gresku split od undefined zato sto saljemo prazne podatke)", function(done) {
    supertest(app)
      .post("/novaRezervacija")
      .send({})
      .expect(500)
      .end(function(err, res) {
        if (err) done(err);
        done();
      });
  });
});

describe("GET /termin", function() {
  it("Status code 200", function(done) {
    supertest(app)
      .get("/termin")
      .expect(200)
      .end(function(err, res) {
        if (err) done(err);
        done();
      });
  });

  it("Provjera kada se baza ucita", function(done) {
    supertest(app)
      .get("/termin")
      .expect([{"redovni":false,"dan":null,"datum":"1.1.2019","semestar":null,"pocetak":"12:00:00","kraj":"13:00:00"},
               {"redovni":true,"dan":0,"datum":null,"semestar":"zimski","pocetak":"13:00:00","kraj":"14:00:00"}])
      .end(function(err, res) {
        if (err) done(err);
        done();
      });  
  });
});

