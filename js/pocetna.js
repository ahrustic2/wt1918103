let ucitaneSlike = [];
let trenutnaStranica = 1;
let brojUcitanihStranica = 1;

function ucitajPocetneSlike()
{
    Pozivi.ucitajPocetneSlikeServer();
}

function sljedeci() 
{
    if (trenutnaStranica >= 4) 
    {
        document.getElementsByClassName("dugmeS").disabled = true;
    }
    else 
    {
        document.getElementById("elementi").innerHTML = "";
        if (trenutnaStranica == brojUcitanihStranica)
        {
            trenutnaStranica++;
            brojUcitanihStranica++;
            document.getElementsByClassName("dugmeS").disabled = false;
            Pozivi.ucitajSlike();
        }
        else 
        {
            trenutnaStranica++;
            let saleSaStranice = mapaUcitanihStranica.get(trenutnaStranica);
            parsiranje(saleSaStranice, 0);
        }
    }
}

function prethodni()
{
    if (trenutnaStranica - 1 <= 0) 
    {
        document.getElementsByClassName("dugmeP").disabled = true;
    }

    else 
    {
        trenutnaStranica--;
        document.getElementById("elementi").innerHTML = "";
        document.getElementsByClassName("dugmeP").disabled = false;
        let saleSaStranice = mapaUcitanihStranica.get(trenutnaStranica);
        parsiranje(saleSaStranice, 0);

    }
}

