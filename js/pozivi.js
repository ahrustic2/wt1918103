let listaPeriodicnihSala = [];
let listaVanrednihSala = [];

let mapaUcitanihStranica = new Map();

let Pozivi = (function(){

    /*function ucitajPodatke() {
        
       listaVanrednihSala = [];
       listaPeriodicnihSala = [];
       let jsonFile = new XMLHttpRequest();
       jsonFile.open("GET", "zauzeca.json", true);
       jsonFile.onreadystatechange = function ()
       {
           if(jsonFile.readyState === 4)
           {
               if(jsonFile.status === 200 || jsonFile.status == 0)
               {
                   let data = JSON.parse(jsonFile.responseText);
                   $.each(data.periodicna, function(){
                    var sala = {dan:this['dan'], semestar:this['semestar'], pocetak:this['pocetak'], kraj:this['kraj'], naziv:this['naziv'], predavac:this['predavac']}
                    listaPeriodicnihSala.push(sala);
                });
                    $.each(data.vanredna, function(){
                    var sala = {datum:this['datum'], pocetak:this['pocetak'], kraj:this['kraj'], naziv:this['naziv'], predavac:this['predavac']}
                    listaVanrednihSala.push(sala);
                });
                let kalendar = document.getElementsByClassName("cal")[0];
                kalendar.innerHTML = "";
                Kalendar.ucitajPodatke(listaPeriodicnihSala, listaVanrednihSala);
                Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
                Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                                document.getElementById("pocetak").value, document.getElementById("kraj").value);
               }
           }
       }
       jsonFile.send(null);
    }*/

    
    
 function upisiZauzeceServer(zauzimanje)
 {
    let file = new XMLHttpRequest(); 
    file.open("POST", "/novaRezervacija", true);
    file.setRequestHeader("Content-Type", "application/json");
    file.send(JSON.stringify(zauzimanje));

    file.onreadystatechange = function () {
        if (file.readyState == 4 && file.status == 200) {
            let jsonText = JSON.parse(file.responseText);
            Pozivi.ucitajRezervacijeIzBaze();
            osvjeziKalendar();
            if (jsonText.dodajSalu == 1){
                alert('Nije moguće rezervisati salu ' + sala.naziv + ' za navedeni datum ' + datumZaDodavanjeDrugiFormat + ' i termin od ' +sala.pocetak+ ' do ' + sala.kraj + 
                      ', sala je rezervisana od strane: '+ jsonText.preda +'!');
                let kalendar = document.getElementsByClassName("cal")[0];
                kalendar.innerHTML = "";
                Pozivi.ucitajRezervacijeIzBaze();
                Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
                Kalendar.ucitajPodatke(listaPeriodicnihSala, listaVanrednihSala);
                Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);
            }
            else if (jsonText.dodajSalu == 3)
            {
                alert('Nije moguće rezervisati periodicnu salu van ljetnog ili zimskog semestra!');
                let kalendar = document.getElementsByClassName("cal")[0];
                kalendar.innerHTML = "";
                Pozivi.ucitajRezervacijeIzBaze();
                Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
                Kalendar.ucitajPodatke(listaPeriodicnihSala, listaVanrednihSala);
                Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);      

            }
            else 
            {
                alert('Uspjesno ste rezervisali termin!');
                let kalendar = document.getElementsByClassName("cal")[0];
                kalendar.innerHTML = "";
                Pozivi.ucitajRezervacijeIzBaze();
                Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
                Kalendar.ucitajPodatke(listaPeriodicnihSala, listaVanrednihSala);
                Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);      

            }
        }
    }
 }

 function ucitajPocetneSlikeServer()
 {
    let jsonFile = new XMLHttpRequest();
    jsonFile.open("GET", "pocetnaStrana", true);
    jsonFile.onreadystatechange = function ()
    {
        if(jsonFile.readyState === 4)
        {
            if(jsonFile.status === 200 || jsonFile.status == 0)
            {
                let podaci = jsonFile.responseText;
                podaci = JSON.stringify(podaci);
                podaci = podaci.replace('"', '');
                let slike = podaci.split(",");
                let podaciSale3 =  slike[2];
                podaciSale3 = podaciSale3.replace('"', '');

                let box = document.createElement('grid-box');
                box.className = "grid-box";
                var img1 = document.createElement('img'); 
                img1.src = '' + slike[0]; 
                box.appendChild(img1);   
                document.getElementById("elementi").appendChild(box);
     
                let box2 = document.createElement('grid-box');
                box2.className = "grid-box";
                var img2 = document.createElement('img'); 
                img2.src = '' + slike[1]; 
                box2.appendChild(img2); 
                document.getElementById("elementi").appendChild(box2);

                let box3 = document.createElement('grid-box');
                box3.className = "grid-box";
                var img3 = document.createElement('img'); 
                img3.src = '' + podaciSale3; 
                box3.appendChild(img3);
                document.getElementById("elementi").appendChild(box3);
                
                let saleString = "" + slike[0] + "," +  slike[1] + "," + podaciSale3;
                mapaUcitanihStranica.set(1, saleString);
            }
        }
    }
    jsonFile.send(mapaUcitanihStranica.size);
 }

 function ucitajSlike()
 {

    let jsonFile = new XMLHttpRequest();
    jsonFile.open("GET", "stranica" + trenutnaStranica, true);
    jsonFile.onreadystatechange = function ()
    {
        if(jsonFile.readyState === 4)
        {
            if(jsonFile.status === 200 || jsonFile.status == 0)
                {   
                    let data = jsonFile.responseText;
                    data = JSON.stringify(data);
                   
                    parsiranje(data, 1);
                }
            }
        }
        jsonFile.send();
    }

    function ucitajPodatkeIzBaze() {
       $.get("/osoblje", function(data, status){
           staviPodatkeUSelect(data);
       })
     }

     function ucitajOsobljeIzBaze() {
        $.get("/osobljeZaText", function(data, status){
            ucitajOsoblje(data);
        })
      }

      function ucitajSaleIOsobe() {
        $.get("/osobljeISale", function(data, status){
            let podaci = [];
            $.each(data.podaci, function(){
                var sala = {ime:this['ime'], prezime:this['prezime'], uloga:this['uloga'], naziv:this['naziv']}
                podaci.push(sala);
            });
            ucitajSaleIOsoblje(podaci);
        })
      }

     function ucitajRezervacijeIzBaze() {
        listaVanrednihSala = [];
        listaPeriodicnihSala = [];
        $.get("/rezervacijeBaza", function(data, status){
            $.each(data.periodicna, function(){
                var sala = {dan:this['dan'], semestar:this['semestar'], pocetak:this['pocetak'], kraj:this['kraj'], naziv:this['naziv'], predavac:this['predavac'], uloga:this['uloga']}
                listaPeriodicnihSala.push(sala);
            });
                $.each(data.vanredna, function(){
                var sala = {datum:this['datum'], pocetak:this['pocetak'], kraj:this['kraj'], naziv:this['naziv'], predavac:this['predavac'], uloga:this['uloga']}
                listaVanrednihSala.push(sala);
            });
            let kalendar = document.getElementsByClassName("cal")[0];
            kalendar.innerHTML = "";
            Kalendar.ucitajPodatke(listaPeriodicnihSala, listaVanrednihSala);
            Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
            Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                            document.getElementById("pocetak").value, document.getElementById("kraj").value);
        })
      }
 

    return {
    //ucitajPodatke: ucitajPodatke,
    upisiZauzeceServer: upisiZauzeceServer,
    ucitajPocetneSlikeServer: ucitajPocetneSlikeServer,
    ucitajSlike : ucitajSlike,
    ucitajPodatkeIzBaze: ucitajPodatkeIzBaze,
    ucitajRezervacijeIzBaze: ucitajRezervacijeIzBaze,
    ucitajOsobljeIzBaze: ucitajOsobljeIzBaze,
    ucitajSaleIOsobe: ucitajSaleIOsobe
    }
    }());

function vratiListuPeriodicnihSala()
{
    return listaPeriodicnihSala;
}

function vratiListuVanrednihSala()
{
    return listaVanrednihSala;
}

function parsiranje(data, ucitavanje)
{
    
    let podaci = data.replace('"', '');
    let slike = podaci.split(",");
    if (slike.length == 3)
    {
        let podaciSale3 = slike[2];
        podaciSale3 = podaciSale3.replace('"', '');

        let box = document.createElement('grid-box');
        box.className = "grid-box";
        var img1 = document.createElement('img'); 
        img1.src = '' + slike[0]; 
        box.appendChild(img1);   
        document.getElementById("elementi").appendChild(box);

        let box2 = document.createElement('grid-box');
        box2.className = "grid-box";
        var img2 = document.createElement('img'); 
        img2.src = '' + slike[1]; 
        box2.appendChild(img2); 
        document.getElementById("elementi").appendChild(box2);

        let box3 = document.createElement('grid-box');
        box3.className = "grid-box";
        var img3 = document.createElement('img'); 
        img3.src = '' + podaciSale3; 
        box3.appendChild(img3);
        document.getElementById("elementi").appendChild(box3);

        if (ucitavanje == 1){
            let saleString = "" + slike[0] + "," + slike[1] + "," + podaciSale3;
            mapaUcitanihStranica.set(trenutnaStranica, "" + saleString);
        }
    }
    else if (slike.length == 2)
    {
        let podaciSale3 =  slike[1];
        podaciSale3 = podaciSale3.replace('"', '');

        let box = document.createElement('grid-box');
        box.className = "grid-box";
        var img1 = document.createElement('img'); 
        img1.src = '' + slike[0]; 
        box.appendChild(img1);   
        document.getElementById("elementi").appendChild(box);

        let box2 = document.createElement('grid-box');
        box2.className = "grid-box";
        var img2 = document.createElement('img'); 
        img2.src = '' + podaciSale3; 
        box2.appendChild(img2); 
        document.getElementById("elementi").appendChild(box2);

        if (ucitavanje == 1){
            let saleString = "" + slike[0] + "," + podaciSale3;
            mapaUcitanihStranica.set(trenutnaStranica, saleString);
        }
    }
    else
    {
        let podaciSale3 =  slike[0];
        podaciSale3 = podaciSale3.replace('"', '');

        let box = document.createElement('grid-box');
        box.className = "grid-box";
        var img1 = document.createElement('img'); 
        img1.src = '' + podaciSale3; 
        box.appendChild(img1);   
        document.getElementById("elementi").appendChild(box);
        
        if (ucitavanje == 1){
            let saleString = "" + podaciSale3;
            mapaUcitanihStranica.set(trenutnaStranica, saleString);
        } 
    }
}