let pomocnaMjesec = new Date().getMonth();
let sala;
let today = new Date();
let trenutnaGodina = 2019;
let datumZaDodavanjeDrugiFormat; 

let ljetniSemestar = ["Februar", "Mart", "April", "Maj", "Juni"];
let zimskiSemestar = ["Januar", "Oktobar", "Novembar", "Decembar"];
let listaMjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
    
let mapaPocetnihDana = new Map([[0,  1], [1,  4], [2,  4], [3,  0], [4,  2], [5,  5], [6,  0], [7, 3], [8,  6],[9,  1], [10,  4], [11,  6]]);
let mapaMjeseciSaBrojemDana = new Map([[0,  31], [1,  28], [2,  31], [3,  30], [4,  31], [5,  30], [6,  31], [7, 31], [8,  30],[9,  31], [10,  30], [11,  31]]);

let unesenPocetak = 0;
let unesenKraj = 0;

let Kalendar = (function(){

function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj){

listaPeriodicnihSala = vratiListuPeriodicnihSala();
listaVanrednihSala = vratiListuVanrednihSala();    

let pocetniDan = mapaPocetnihDana.get(mjesec);
let salaIzRezervacije = sala;
if (pocetak != 0 && kraj != 0)
{
        for (let i = 0; i < listaPeriodicnihSala.length; i++)
        {   
            if (validirajVrijednostiPeriodicna(listaPeriodicnihSala[i].dan, listaPeriodicnihSala[i].pocetak, listaPeriodicnihSala[i].kraj) == 1){
            let zauzeta = unutarVremena(pocetak, kraj, listaPeriodicnihSala[i].pocetak, listaPeriodicnihSala[i].kraj);
            if (zauzeta == 1)
            {
                let prviDanZaBojiti = izracunajPocetniDan(pocetniDan, listaPeriodicnihSala[i].dan);
            
                if (listaPeriodicnihSala[i].semestar == "zimski"){
                    if (listaPeriodicnihSala[i].naziv == salaIzRezervacije){
                    for (let m = 0; m < zimskiSemestar.length; m++) 
                    {
                        if (listaMjeseci[mjesec] == zimskiSemestar[m])
                        {
                            let celija = document.getElementsByClassName("bojaCelije");
                            
                            for (let v = prviDanZaBojiti; v < celija.length; v+=7)
                            {
                                celija[v].children[0].className = "zauzeta";
                            }
                        
                        }
                    }
                }
            }
                else if (listaPeriodicnihSala[i].semestar == "ljetni")
                {
                    for (let m = 0; m < ljetniSemestar.length; m++) 
                    {
                        if (listaMjeseci[mjesec] == ljetniSemestar[m])
                        {
                            let celija = document.getElementsByClassName("bojaCelije");
                            for (let v = prviDanZaBojiti; v < celija.length; v+=7)
                            {
                                celija[v].children[0].className = "zauzeta";
                            } 
                        }
                    }  
                }
            }
        }
    }

    for (let i = 0; i < listaVanrednihSala.length; i++)
    {
        if (validirajVrijednostiNeperiodicna(listaVanrednihSala[i].datum, listaVanrednihSala[i].pocetak, listaVanrednihSala[i].kraj) == 1)
        {
            let zauzeta = unutarVremena(pocetak, kraj, listaVanrednihSala[i].pocetak, listaVanrednihSala[i].kraj);
            if (zauzeta == 1)
            {
                if (listaVanrednihSala[i].naziv == salaIzRezervacije)
                {
                    let datumSale = listaVanrednihSala[i].datum;
                    let pom1 = datumSale.split(".");
                    let danSale = pom1[0];
                    danSale--;
                    let mjesecSale = pom1[1];
                    mjesecSale--;
                    if (pomocnaMjesec == mjesecSale)
                    {
                        let celija = document.getElementsByClassName("bojaCelije");
                        celija[danSale].children[0].className = "zauzeta";
                    }
                }
            }
        }
    }
}
}
function ucitajPodatkeImpl(periodicna, redovna)
{   
    listaPeriodicnihSala = periodicna.slice();
    listaVanrednihSala = redovna.slice();
}

function iscrtajKalendarImpl(kalendarRef, mjesec)
{
    
    let listaDana = ["PON", "UTO", "SRI", "CET", "PET", "SUB", "NED"];
    trenutnaGodina = 2019;
    let firstDay = (new Date(trenutnaGodina + "-" + mjesec + "-01")).getDay()+2;
    let daysInMonth = 32 - new Date(trenutnaGodina, mjesec, 32).getDate();
    let date = 1;
    let tabela1 = document.createElement("table");
    tabela1.className="tabela1";
    if ( mjesec == 4 || mjesec == 6 || mjesec == 9 || mjesec == 11) firstDay--;
    if (mjesec == 2) firstDay -= 3;
    if (mjesec == 0) firstDay = 1;
    if ((mjesec == 0 || mjesec == 2 ) && trenutnaGodina == 2020) firstDay++;
    if (mjesec == 8 && trenutnaGodina == 2020) firstDay = 1;
    for (let i = 1; i < 9; i++){
        var red = document.createElement("tr");
        for (let j = 0; j < 7; j++){
             if (i == 1){
                let mjesec2 = document.createElement("td");
                mjesec2.className="mjesec";
                if (j == 0){
                    let tekstMjesec = document.createTextNode(listaMjeseci[mjesec]);
                    mjesec2.appendChild(tekstMjesec);
                }
                red.appendChild(mjesec2);
            }

            else if (i == 2){
                let dani = document.createElement("td");
                dani.className = "dani";
                let tekstDani = document.createTextNode(listaDana[j]);
                dani.appendChild(tekstDani);
                red.appendChild(dani);
            }

            else if (i == 3 && j < firstDay) {
                let cell = document.createElement("td");
                let cellText = document.createTextNode("");
                cell.appendChild(cellText);
                red.appendChild(cell);
                tabela1.appendChild(red);
            }
           
            else {
                let celija = document.createElement("td");
                let tabela2 = document.createElement("table");
                tabela2.className="tabela2";
                celija.appendChild(tabela2);
                for  (let m = 0; m < 2; m++){
                    var redU = document.createElement("tr");
                    
                        if (m==0){
                            let kolonaU = document.createElement("td");
                            let danUKal = document.createTextNode(date);
                            kolonaU.appendChild(danUKal);
                            redU.className = "brojDana";
                            redU.appendChild(kolonaU);
                            date++;
                        }
                        else if (m==1){
                            let kolonaU2 = document.createElement("td");
                            kolonaU2.className = "slobodna";
                            redU.className = "bojaCelije";
                            redU.appendChild(kolonaU2);
                        }
                        tabela2.appendChild(redU);  
                }
                if (date-1 > daysInMonth) {
                    break;
                }
                celija.appendChild(tabela2);
                red.appendChild(celija);
            }
        }
       tabela1.appendChild(red);
    }
    kalendarRef.appendChild(tabela1);

    //Otvaranje prozora na odabir dana u kalendaru
    document.querySelectorAll('.tabela2 td')
    .forEach(e => e.addEventListener("click", function() {
    
        let mjesecZaDodavanje  = pomocnaMjesec + 1;
        let salaZaDodavanje = document.getElementsByClassName("saleIzbor")[0].value;
        let pocetakZaDodavanje = document.getElementById("pocetak").value;
        let krajZaDodavanje = document.getElementById("kraj").value;
        let today = new Date();
        //0 da nije periodicna, 1 za periodicna
        let periodicnaRezervacija = 0; 

        //Da li su podaci popunjeni
        if (pocetakZaDodavanje != "" && krajZaDodavanje != "")
        { 
            let dan = e.innerHTML;
            if (dan != "")
            {
                if (document.getElementById("periodicna").checked) periodicnaRezervacija = 1;
                else periodicnaRezervacija = 0;
                let datumZaDodavanje = dan + "." + mjesecZaDodavanje + "." + "2019";
                datumZaDodavanjeDrugiFormat = dan + "/" + mjesecZaDodavanje + "/" + "2019";
                let predavacIzRezervacije = document.getElementsByClassName("osobljeIzbor")[0].value;
                let pom13 = predavacIzRezervacije.split(" ");
                let imeIzbor = pom13[0];
                let preIzbor = pom13[1];
                let idO;
                let uloga;
                for (let i = 0; i < mapaOsobljaIzBaze.size; i++)
                {
                    let osoba = mapaOsobljaIzBaze.get(i);
                    let pom = osoba.split(",");
                    let ime = pom[0];
                    let pre = pom[1];
                    uloga = pom[2];
                    idO = i+1;
                
                if (ime == imeIzbor && preIzbor == pre){break;} 

                }
                if (validirajVrijednostiNeperiodicna(datumZaDodavanje, pocetakZaDodavanje, krajZaDodavanje) == 1)
                {
                    sala = {"periodicnaRezervacija" : periodicnaRezervacija, "datum":datumZaDodavanje, 
                                "pocetak":pocetakZaDodavanje, "kraj":krajZaDodavanje, "naziv":salaZaDodavanje, 
                                "predavacIme": imeIzbor, "predavacPrezime": preIzbor, "uloga": uloga, "idOsobe": idO};

                    let validacija = validacijaSaleKlijent(sala);
                                
                    if (validacija.dodajSalu == 0)
                    {
                        if (confirm("Da li želite da rezervisati ovaj termin?") == true) 
                        {
                            upisiZauzece(sala);
                        }
                    }
                    else if (validacija.dodajSalu == 3)
                    {
                        alert('Nije moguće rezervisati periodicnu salu van ljetnog ili zimskog semestra!');
                        osvjeziKalendar();
                    }
                    else
                    {
                        alert('Nije moguće rezervisati salu ' + sala.naziv + ' za navedeni datum ' + datumZaDodavanjeDrugiFormat + ' i termin od ' +sala.pocetak+ ' do ' + sala.kraj + 
                            ', sala je rezervisana od strane: '+ validacija.preda + '!');
                        osvjeziKalendar();
                    }
                }
            }
        }
    }));
}
return {
obojiZauzeca: obojiZauzecaImpl,
ucitajPodatke: ucitajPodatkeImpl,
iscrtajKalendar: iscrtajKalendarImpl
}
}());

function next() {
    pomocnaMjesec++;
    if (pomocnaMjesec < 12){
        
        document.getElementsByClassName("dugmeS").disabled = false;
        let today = new Date();
        let pozicija = today.getMonth();
        let kalendar = document.getElementsByClassName("cal")[0];
        kalendar.innerHTML = "";
        let trenutniMjesec= new Date().getMonth();
        Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
        Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);

    }
    else {
        pomocnaMjesec = 11;
        document.getElementsByClassName("dugmeS").disabled = true;
    }
}

function previous() {
    pomocnaMjesec--;
    if (pomocnaMjesec >= 0){
        document.getElementsByClassName("dugmeP").disabled = false;
        let today = new Date();
        let kalendar = document.getElementsByClassName("cal")[0];
        kalendar.innerHTML = "";
        let trenutniMjesec= new Date().getMonth();
        Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
        Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);
    }
    else {
        pomocnaMjesec = 0;
        document.getElementsByClassName("dugmeP").disabled = true;
    }
}

function izracunajPocetniDan(pocetniDan, danListe){
    let prviDanZaBojiti = danListe;
            if (pocetniDan == 6 && danListe != 6) 
            {
                prviDanZaBojiti = prviDanZaBojiti + 1;
            }
            else if (pocetniDan > danListe)
            {
                prviDanZaBojiti = danListe + pocetniDan - 1;
            }
            else if (danListe > pocetniDan) 
            {
                prviDanZaBojiti = danListe - pocetniDan;
            }
            else if (danListe == pocetniDan)
            {
                prviDanZaBojiti = 0;
                if (pocetniDan - 7 > 0) pocetniDan -= 7; 
            }
            if ((danListe == 0 && (pomocnaMjesec == 9 || pomocnaMjesec == 0))) 
            {
                prviDanZaBojiti--;
                prviDanZaBojiti += 7;
            }
            if (pocetniDan == 0)
            {
                prviDanZaBojiti = danListe;
            }
            if (pomocnaMjesec == 5) prviDanZaBojiti -= 2;
            if (pomocnaMjesec == 5 && (danListe == 5 || danListe == 6)) prviDanZaBojiti += 2;
            if (pomocnaMjesec == 4 && (danListe == 0 || danListe == 1)) prviDanZaBojiti += 4;
            if (trenutnaGodina == 2020) 
            {
                prviDanZaBojiti = prviDanZaBojiti - 2;
                if (prviDanZaBojiti < 0) prviDanZaBojiti +=7;
                if (pomocnaMjesec == 11 && danListe == 0) prviDanZaBojiti = 0;
                if (pomocnaMjesec == 0) prviDanZaBojiti ++;
                if (pomocnaMjesec == 1) prviDanZaBojiti ++;
            }
            return prviDanZaBojiti;
}


function unutarVremena(pocetak, kraj, salaPocetak, salaKraj)
{
    let zauzeta = 0;
    if (pocetak > salaPocetak && pocetak < salaKraj) 
         zauzeta = 1;
    else if (pocetak < salaPocetak && kraj > salaPocetak)
        zauzeta = 1;
    else if (pocetak == salaPocetak || kraj == salaKraj)
        zauzeta = 1;    
    return zauzeta;    
}

function unesenoVrijemeZaPocetak()
{
    unesenPocetak = 1;
}

function unesenoVrijemeZaKraj()
{
    unesenKraj = 1;
}

function validirajVrijednostiPeriodicna(danZaValidaciju, pocetakZaValidaciju, krajZaValidaciju)
{
    if (danZaValidaciju > 6 || danZaValidaciju < 0) return 0;
    
    var satiPocetak = pocetakZaValidaciju.split(":")[0];
    var minutePocetak = pocetakZaValidaciju.split(":")[1];

    var satiKraj = krajZaValidaciju.split(":")[0];
    var minuteKraj = krajZaValidaciju.split(":")[1];

    if (satiPocetak > satiKraj) return 0;
    if (satiPocetak > 24 || satiPocetak < 0) return 0;
    if (satiKraj > 24 || satiKraj < 0) return 0;

    return 1;
}

function validirajVrijednostiNeperiodicna(datumZaValidaciju, pocetakZaValidaciju, krajZaValidaciju)
{
    let pom1 = datumZaValidaciju.split(".");
    let danSale = pom1[0];
    let mjesecSale = pom1[1];
    mjesecSale--;
    mjesecSale = parseInt(mjesecSale);
    let brojDanaMjeseca = mapaMjeseciSaBrojemDana.get(mjesecSale);
    
    if (mjesecSale > 12 || mjesecSale < 0) return 0;
    if (danSale > brojDanaMjeseca) return 0;

    var satiPocetak = pocetakZaValidaciju.split(":")[0];
    var minutePocetak = pocetakZaValidaciju.split(":")[1];

    var satiKraj = krajZaValidaciju.split(":")[0];
    var minuteKraj = krajZaValidaciju.split(":")[1];

    if (satiPocetak > satiKraj) return 0;
    if (satiPocetak > 24 || satiPocetak < 0) return 0;
    if (satiKraj > 24 || satiKraj < 0) return 0;

    return 1;
}

function validacijaSaleKlijent(salaKojuValidiramo)
{  
        let dodajSalu = 0;
        let pom1 = salaKojuValidiramo.datum.split(".");
        let danSaleKojaSeDodaje = pom1[0];
        mjesecSale = pom1[1];
        let pom2Mjesec = mjesecSale;
        mjesecSale--;
        mjesecSale = parseInt(mjesecSale);
        pom2Mjesec = parseInt(pom2Mjesec);
        godina = 2019;
        let predavac = "";

        let semestarRezervacije = "";
        if(mjesecSale >=1 && mjesecSale <= 5) {
            semestarRezervacije = "ljetni";
        } else if(mjesecSale == 0 || (mjesecSale >= 9 && mjesecSale <= 11)) {
            semestarRezervacije = "zimski";
        }
        
        let noviDatum = pom2Mjesec + "." + danSaleKojaSeDodaje + "." + godina;
        let datumPomocna = new Date(noviDatum);
        let danSale = datumPomocna.getDay();
        danSale = dajDan(danSale);
        
        for (let i = 0; i < listaVanrednihSala.length; i++)
        {
            //za validaciju vanredne i vanredne
            if (listaVanrednihSala[i].naziv == salaKojuValidiramo.naziv && listaVanrednihSala[i].datum == salaKojuValidiramo.datum)
            {
                //koristim funkciju iz kalendara, ako se vrijeme ne poklapa dodamo 
                if (unutarVremena(salaKojuValidiramo.pocetak, salaKojuValidiramo.kraj, listaVanrednihSala[i].pocetak, listaVanrednihSala[i].kraj) == 1)
                {
                    predavac = listaVanrednihSala[i].predavac + " ( " + listaVanrednihSala[i].uloga + " ) ";
                    dodajSalu = 1;
                    break;
                    
                }
            }
            
            //prolazi kroz listu vanrednih i provjerava na koji su dan rezervisane
            if (salaKojuValidiramo.periodicnaRezervacija == 1)
            {
                if (listaVanrednihSala[i].naziv == salaKojuValidiramo.naziv)
                {
                    
                    let p1 = listaVanrednihSala[i].datum.split(".");
                    let dan1 = p1[0];
                    let mjesec1 = p1[1];
                    mjesec1 = parseInt(mjesec1);
                    let godina1 = 2019;

                    let novi1 = mjesec1 + "." + dan1 + "." + godina1;
                    let novi1Pom = new Date(novi1);
                    let dan2 = novi1Pom.getDay();
                    dan2 = dajDan(dan2);
        
                    let semestarSaleIzListe = "";
                    if(mjesec1 >=2 && mjesec1 <= 6) {
                        semestarSaleIzListe = "ljetni";
                    } else if(mjesec1 == 1 || (mjesec1 >= 10 && mjesec1 <= 12)) {
                        semestarSaleIzListe = "zimski";
                    }

                    if (unutarVremena(salaKojuValidiramo.pocetak, salaKojuValidiramo.kraj, listaVanrednihSala[i].pocetak, listaVanrednihSala[i].kraj) == 1)
                    {
                        if (dan2 == danSale && semestarRezervacije == semestarSaleIzListe)
                        {
                            predavac = listaVanrednihSala[i].predavac + " ( " + listaVanrednihSala[i].uloga + " ) ";
                            dodajSalu = 1;
                            break;
                        } 
                    }
                }
            }
        }

        //Ako je periodicna i van semestara
        if (salaKojuValidiramo.periodicnaRezervacija == 1)
        {
            if (semestarRezervacije == "")
            {
                dodajSalu = 3;
            }    
        }

    
        for (let j = 0; j < listaPeriodicnihSala.length; j++)
        {
            //provjera naziva sala
            if (listaPeriodicnihSala[j].naziv == salaKojuValidiramo.naziv && listaPeriodicnihSala[j].semestar == semestarRezervacije)
            {
                    //provjera da li se vrijeme poklapa
                    if (unutarVremena(salaKojuValidiramo.pocetak, salaKojuValidiramo.kraj, listaPeriodicnihSala[j].pocetak, listaPeriodicnihSala[j].kraj) == 1)
                    {
                        //provjera da li su na isti dan unutar sedmice
                        if (danSale == listaPeriodicnihSala[j].dan)
                        {
                            predavac = listaPeriodicnihSala[j].predavac + " ( " + listaPeriodicnihSala[j].uloga + " ) ";
                            dodajSalu = 1;
                            break;
                        }
                    }
            }
        }

        return {dodajSalu: dodajSalu, preda: predavac};
}

function dajDan(dan)
{
    if(dan == 0) 
        {
            dan = 6;
        } else {
            dan--;
        }

        return dan;
}