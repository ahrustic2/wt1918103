function ucitajKalendar(){
    Pozivi.ucitajPodatkeIzBaze();
    //Pozivi.ucitajPodatke();
    Pozivi.ucitajRezervacijeIzBaze();
    let kalendar = document.getElementsByClassName("cal")[0];
    kalendar.innerHTML = "";
    Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
    Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                    document.getElementById("pocetak").value, document.getElementById("kraj").value);
}

function upisiZauzece(sala)
{
   Pozivi.upisiZauzeceServer(sala);
}


function osvjeziKalendar()
{
        let kalendar = document.getElementsByClassName("cal")[0];
        kalendar.innerHTML = "";
        //Pozivi.ucitajPodatke();
        Pozivi.ucitajRezervacijeIzBaze();
        Kalendar.iscrtajKalendar(kalendar, pomocnaMjesec);
        Kalendar.ucitajPodatke(listaPeriodicnihSala, listaVanrednihSala);
        Kalendar.obojiZauzeca(document.getElementsByClassName("cal"), pomocnaMjesec, document.getElementsByClassName("saleIzbor")[0].value, 
                        document.getElementById("pocetak").value, document.getElementById("kraj").value);
}

let mapaOsobljaIzBaze = new Map();

function staviPodatkeUSelect(podaci)
{
   var selectOsobe = document.getElementsByClassName("osobljeIzbor")[0];
   for ( let i = 0; i < podaci.length; i++)
   {
      var option = document.createElement("option");
      option.text = "" + podaci[i].ime + " " + podaci[i].prezime;
      option.value = "" + podaci[i].ime + " " + podaci[i].prezime;
      selectOsobe.appendChild(option);
      let osoba = podaci[i].ime + "," + podaci[i].prezime + "," + podaci[i].uloga;
      mapaOsobljaIzBaze.set(i, osoba);
      
   }
}
