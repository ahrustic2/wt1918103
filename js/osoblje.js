let osoblje = [];
function ucitajOsoblje(podaci)
{   
    osoblje = [];
    for ( let i = 0; i < podaci.length; i++)
    {
       osoblje.push({ime: podaci[i].ime, prezime: podaci[i].prezime, uloga: podaci[i].uloga});
    }
}

function ucitajSaleIOsoblje(podaci)
{
    let text = "";
    $("#textOsoblje").html(text);
    let osobljeUSali = false;
    for ( let i = 0; i < osoblje.length; i++)
    {
        for ( let j = 0; j < podaci.length; j++)
        {
            if (osoblje[i].ime == podaci[j].ime && osoblje[i].prezime == podaci[j].prezime)
            {
                text = text + podaci[j].ime + " " + podaci[j].prezime + " (" + podaci[j].uloga + ") : " +  podaci[j].naziv + "<br />" ;
                osobljeUSali = true;
            }
        }
        if (osobljeUSali == false)
        {
            text = text + osoblje[i].ime + " " +  osoblje[i].prezime + " (" +  osoblje[i].uloga + ") : u kancelariji "+ "<br />" ; 
        }
        osobljeUSali = false;
    }
    $("#textOsoblje").html(text);
}

function ucitaj()
{
    Pozivi.ucitajOsobljeIzBaze();
    Pozivi.ucitajSaleIOsobe();
    setInterval(function(){ ucitajZaInterval(); }, 30000);
}

function ucitajZaInterval()
{
    Pozivi.ucitajOsobljeIzBaze();
    Pozivi.ucitajSaleIOsobe();
}
